# Dev notes

## Массив со всеми сериями
`Array.from(document.querySelectorAll('.page_nav > a')).map(a => a.href)`

> (64) ["http://cartoonsub.com/oneseries.php?id_series=1716&id_serials=49", "http://cartoonsub.com/oneseries.php?id_series=1716&id_serials=49", "http://cartoonsub.com/oneseries.php?id_series=1715&id_serials=49", "http://cartoonsub.com/oneseries.php?id_series=1715&id_serials=49", "http://cartoonsub...

## Название сезона
`document.querySelector('.navifationPath').innerHTML.split('→').slice(-1)[0].trim()`

> "3 сезон"

## Название сериала
`Array.from(document.querySelectorAll('.navifationPath a')).filter(a => a.href.includes('id_serials='))[0].title`

> "star vs the forces of evil | Стар против сил зла"

## Можно скомпоновать в обьект типа

```
{
    season: "3 сезон",
    name: "star vs the forces of evil | Стар против сил зла",
    episodes: urls.map(u => await getEpisode(u))
}
```