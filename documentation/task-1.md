# Dev notes

## Получение всех урлов видео, разынх озвучек и субтитров
`Array.from(document.querySelectorAll('.contentVideo iframe')).map(f => f.src)`

> ["https://www.stormo.tv/embed/214491/", "https://www.stormo.tv/embed/211536/", "https://www.stormo.tv/embed/211504/"]

## Алеасы к ним
`Array.from(document.querySelectorAll('.contentnav a')).map(n => n.innerText.trim())`

>  ["Субтитры", "Озвучка", "озвучка 2"]

## Название мульта, название серии
`document.querySelector('.content h1').innerText.trim()`

> "8 серия 3 сезона Scent of a Hoodie | Запах толстовки"

# Как вариант скомпоновать все это в обьект серии

```
{
    links: {
        'Субтитры': 'https://www.stormo.tv/embed/214491/',
        'Озвучка': 'https://www.stormo.tv/embed/211536/', 'озвучка 2': 'https://www.stormo.tv/embed/211504'
    },
    title: '8 серия 3 сезона Scent of a Hoodie | Запах толстовки'
}
```

```
function getEpisode() {
    const alias = Array.from(document.querySelectorAll('.contentnav a')).map(n => n.innerText.trim())
    const links = Array.from(document.querySelectorAll('.contentVideo iframe')).map(f => f.src)
    const title = document.querySelector('.content h1').innerText.trim()
    const episode = { links: {}, title }

    alias.forEach((a, i) => episode.links[a] = links[i])
    return episode
}
```