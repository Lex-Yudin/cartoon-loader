# Dev notes
Когда исследовал, что делайет сайт с загрузкой по урлу, обратил внимание, что он стучится к loadercdn.io.
Меня это очень заинтересовало, оказывается, у них есть бесплатные API для такого рода действий, я зарегестрировался и получил серверный ключь.


## Запрос

Получить всю инфу по видео можно посылая запрос на `https://loadercdn.io/api/v1/create`
таким телом: 
```
{
	"key": "lR2QIZSt11xJsk8vl4CfhIMIiLfWXz6YN8X9tAr",
	"link": "https://www.stormo.tv/embed/292200/"
}
```

## Ответ

В ответ приходит обьект типа:
```
{
    "id": "6YN9mEc9h",
    "originalFormat": "mp4",
    "wantedFormat": "mp4",
    "originalType": "video",
    "wantedType": "video",
    "hostname": "stormo.tv",
    "link": "https://www.stormo.tv/embed/292200/",
    "title": "Star.vs.the.Forces.of.Evil.S03E21.Conquer",
    "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.mp4",
    "artist": "www.stormo.tv",
    "track": "Star.vs.the.Forces.of.Evil.S03E21.Conquer",
    "uploader": "www.stormo.tv",
    "suggestions": [
        "Star.vs.the.Forces.of.Evil.S03E21.Conquer",
        "www.stormo.tv"
    ],
    "formats": [
        {
            "format": "mp4",
            "type": "video",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=mp4",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.mp4"
        },
        {
            "format": "avi",
            "type": "video",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=avi",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.avi"
        },
        {
            "format": "flv",
            "type": "video",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=flv",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.flv"
        },
        {
            "format": "webm",
            "type": "video",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=webm",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.webm"
        },
        {
            "format": "gif",
            "type": "video",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=gif",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.gif"
        },
        {
            "format": "mp3",
            "type": "audio",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=mp3",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.mp3"
        },
        {
            "format": "ogg",
            "type": "audio",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=ogg",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.ogg"
        },
        {
            "format": "flac",
            "type": "audio",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=flac",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.flac"
        },
        {
            "format": "wav",
            "type": "audio",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=wav",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.wav"
        },
        {
            "format": "m4a",
            "type": "audio",
            "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=m4a",
            "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.m4a"
        }
    ],
    "url": "https://loadercdn.io/download?id=6YN9mEc9h",
    "headers": {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0 (Chrome)",
        "Cookie": "PHPSESSID=cbvlotcfmpeho07npevn7kd9c3"
    }
}
```
Как я понял, первым в `formats` идет оригинальное разрешение, как правило mp4, что мне вполне подходит, так что, думаю, надо просто брать `formats[0]`

```
{
    "format": "mp4",
    "type": "video",
    "url": "https://loadercdn.io/download?id=6YN9mEc9h&format=mp4",
    "filename": "Star.vs.the.Forces.of.Evil.S03E21.Conquer.mp4"
}
```

Тут выдергиваем `url` и скачиваем