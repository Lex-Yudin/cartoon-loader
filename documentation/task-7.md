# Dev notes
Как я для себя определил, преоритет у меня такой:
субтитры => оригинал => null

нужно сделать функцию универсальной, чтобы можно было подстраивать под любые предподчения, так что нужно это в входные данные

думаю сделать так:
```
{
    primary: '',
    okey: []
}
```

> Тут я указываю необходимую мне дорожку со звуком в primary, а остальные, на мой счет удобоваримые, в okay

> Primary может быть `null`

#### Для моих предпочтений входные данные будут такими
```
{
    primary: 'субтитры',
    okey: ['оригинал']
}
```

## Описание вывода

Если нет ни primary, ни любого okay, то все плохо, нужно вывести ошибку, что нужно искать вручную:

`К сожалению, серии ${episode.title} нет в озвучках ${[options.primary, ...options.okey].join(', ')}`

Если видео нет в primary, но есть в okey, то показываем предупреждение:

`Для серии ${episode.title} нет озвучки ${options.primary}, зато нашлась ${options.okey[i]}`

Если найдена Primary, то все отлично

`Для серии ${episode.title} существует предпочтительная озвучка ${options.primary}`

# Что функция отдает назад?

Функция должна возврашать назад эпизоды с полем selectedLink, в котором будет лежать обьект ссылки на видео `{ 'субтитры': 'http:...' }`, подобранный способами выше