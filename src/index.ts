import { EnvironmentManager } from './environments/environment-manager'
import { Server } from './server'

const environmentManager = new EnvironmentManager(process.env.PRESET || 'dev')
environmentManager.load()

export const server = new Server()
server.start()