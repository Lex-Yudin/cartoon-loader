import * as fs from 'fs'
import * as path from 'path'

import { colors } from '../utils'

export class EnvironmentManager {
  private dir: string = __dirname
  private name: string
  
  constructor(name) { this.name = name }

  load(name?: string): void {
    if (name) this.name = name

    const filePath: string = path.join(this.dir, `${this.name}.json`)

    if (!fs.existsSync(filePath)) {
      this.err(`Пресета с таким именем '${this.name}' не существует`)
      this.err(`Проверьте наличие файла '${filePath}'`)
      console.log()
      return
    }

    console.log()

    try {
      const fileContent: string = fs.readFileSync(filePath, { encoding: 'utf-8' })
      const envs: any = JSON.parse(fileContent)

      for (const env of Object.keys(envs)) {
        const val = envs[env]

        if (typeof process.env[env] !== 'undefined') {
          this.warn(`Параметр '${env}' уже сушествует и равен '${process.env[env]}'`)
          continue
        }

        process.env[env] = val
        this.info(`Параметру '${env}' присвоено значение '${val}'`)
      }
    } catch(err) {
      this.err(`Файл '${filePath}.json' поврежден!`)
      this.err(`Проверьте правильность и валидность JSON файла`)
    }

    console.log()
  }

  err(text: string) {
    console.log(colors('red', text))
  }

  warn(text: string) {
    console.log(colors('yellow', text))
  }

  info(text: string) {
    console.log(colors('green', text))
  }
}