export interface Task {
  id: number
  procent: number
  status: 'started' | 'done' | 'rejected' | 'progress'
  meta?: any,
  error: null | string
}