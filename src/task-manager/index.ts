import { Task } from './models'

export class TaskManager {
  public tasks: Task[] = []
  private onChange: Function

  constructor(onChange: Function) {
    this.onChange = onChange
  }
  
  createTask(callback: (resolve: Function, reject: Function, onProgress?: (procent: number) => void) => void, meta?: any): Task {
    const task: Task = { id: this.tasks.length, meta, procent: 0, status: 'started', error: null }
    this.tasks.push(task)

    const resolve = () => {
      task.status = 'done'
      task.procent = 100
    }

    const reject = (error: string) => {
      task.status = 'rejected'
      task.error = error
    }

    const onProgress = (procent) => {
      task.status = 'progress'
      task.procent = procent

      this.onChange(task)
    }

    // TODO: Add to DB before & give an id to task
    callback(resolve, reject, onProgress)
    this.onChange(task)

    return task
  }
}

/*


TaskManager.createTask((resolve, reject, onProgress) => {

})

*/