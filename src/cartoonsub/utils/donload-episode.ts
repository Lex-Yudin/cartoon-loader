import * as fs from 'fs'
import * as path from 'path'
import * as request from 'request'

import { Episode, FileInfo, Progress } from '../models'
import { getRealUrl } from '../api'
import { makeFileName } from './'

export function downloadEpisode(token: string, episode: Episode, dir: string, customFileName?: string, onProgress?: (progress: Progress) => void): Promise<FileInfo> {
  return new Promise(async resolve => {
    const link: string = episode.selectedLink.url

    const { url, format, headers } = await getRealUrl(link, token)

    const fileName = makeFileName(episode, format)
    const filePath = path.join(dir, customFileName || fileName);

    let downloaded = 0
    let contentLength = 0

    const file = { url, format, fileName: fileName || customFileName, filePath };
    
    const req = request({ uri: url, headers })
      req.pipe(fs.createWriteStream(filePath))
      req.on('response', (data) => contentLength = parseInt(data.headers['content-length']))
      
      if (onProgress) {
        req.on('data', (chunk) => {
          downloaded += chunk.length
          
          const procent = downloaded / contentLength * 100
          const progress: Progress = {
            procent,
            title: episode.title,
            fileInfo: file,
            count: downloaded,
            max: contentLength
          }

          onProgress(progress)
        })
      }
      
      req.on('close', () => resolve(file));
      req.on('end', () => resolve(file));
  });
}
