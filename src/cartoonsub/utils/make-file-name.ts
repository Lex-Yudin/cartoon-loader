import { Episode } from '../models'

export function makeFileName(episode: Episode, format: string): string {
  let title = episode.title.replace(/[^A-zА-я0-9]/g, '_')
  title += `(${Object.keys(episode.selectedLink)[0]})`
  title += `.${format}`

  return title
}
