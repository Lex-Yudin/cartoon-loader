import { colors } from '../../utils'
import { Episode, Link, AnalizeOptions } from '../models'

export function analize(episodes: Episode[], {
  onlyStormo = false,
  filter = false,
  errorLvl = 0,
  ...options
}: AnalizeOptions = { primary: null, okey: [] }): Episode[] {
  // Формируем массив для поиска
  const serach: Array<string> = []
  if (options.primary !== null) serach.push(options.primary.toLowerCase())
  if (options.okey) serach.push(...options.okey.map(ok => ok.toLowerCase()))

  // Если этот массив пуст, то искать нечего
  if (serach.length === 0) {
    console.log(colors('red', 'Необходимо указать предпочтения'));
    return episodes;
  }
    

  const result = []

  // Проходим по всем эпизодам
  for(const episodeInput of episodes) {
    // Делаем копию
    const episode: Episode = Object.assign({}, episodeInput)

    // Ищем хоть какую подпочтительную озвучку в эпизоде
    let match: Link = episode.links.find((link: Link) => {
      let valid = serach.indexOf(link.type) !== -1
      
      if (onlyStormo) {
        const url = link.url
        const isStormo = url.includes('stormo.tv');

        valid = valid && isStormo
      }  

      return valid
    })

    // Если такая есть
    if (match) {
      // То добавляем эту озвучку к эпизоду, как выбранную
      episode.selectedLink = match

      // Если это не Primary озвучка, то предупреждаем, что нашли альтернативу
      if (match.type !== options.primary) {
        if (errorLvl <= 1) console.log(colors('yellow', `* Для серии '${episode.title}' нет озвучки '${options.primary}', зато нашлась '${match.type}'`))
      } else {
        if (errorLvl == 0) console.log(colors('green', `* Для серии '${episode.title}' существует предпочтительная озвучка '${options.primary}'`))
      }
    } else {
      episode.selectedLink = null

      // Ничего не найдено, говорим об этом :(
      if (errorLvl <= 2) console.log(colors('red', `* К сожалению, серии "${episode.title}" нет в озвучках: ${[options.primary, ...options.okey].join(', ')}`))
    }

    // Добавляем в результат, если необходимо отфильтровать и нужная ссылка не найдена - пропускаем
    if (!match && filter) continue;
    result.push(episode)
  }

  // Возвращаем эпизоды назад
  return result
}
