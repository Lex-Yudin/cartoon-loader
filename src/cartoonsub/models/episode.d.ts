import { Link } from './link';

export interface Episode {
  links: Link[]
  title: string
  selectedLink?: Link
  art?: string
}