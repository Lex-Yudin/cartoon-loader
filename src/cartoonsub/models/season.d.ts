import { Episode } from './episode'

export interface Season {
  episodes: Episode[]
  episodeUrls?: string[]
  name: string
  title: string
}