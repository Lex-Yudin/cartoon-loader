import { Episode } from '.';

export interface FileInfo {
  url: string,
  fileName: string,
  format: string,
  headers?: object,
  filePath?: string,
  episode?: Episode
}