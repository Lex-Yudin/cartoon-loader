export interface AnalizeOptions {
  primary: string
  okey: Array<string>

  onlyStormo?: boolean
  filter?: boolean
  errorLvl?: 0 | 1 | 2 | 3
}