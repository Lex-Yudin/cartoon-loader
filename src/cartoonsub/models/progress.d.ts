import { FileInfo } from '.'

export interface Progress {
  title: string
  procent: number

  fileInfo?: FileInfo
  count: number
  max: number
}