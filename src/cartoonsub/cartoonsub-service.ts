import * as api from './api'
import * as utils from './utils'

import { Season, Episode, FileInfo, Progress, AnalizeOptions } from './models'

export class CartoonsubService {
  // Токен для получения "реальной" ссылки на видео
  public loadercdnToken = null
  
  constructor(loadercdnToken) {
    this.loadercdnToken = loadercdnToken
  }

  loadSeason(url): Promise<Season> {
    return api.getSeason(url, false)
  }

  async loadEpisodes(season: Season): Promise<Season> {
    for (const episodeUrl of season.episodeUrls) {
      const episode = await api.getEpisode(episodeUrl)
      season.episodes.push(episode)
    }

    return season
  }

  loadEpisode(url: string): Promise<Episode> {
    return api.getEpisode(url)
  }

  analizeEpisode(options: AnalizeOptions, episodes: Episode[]): Episode[] {
    return utils.analize(episodes, options)
  }

  downloadEpisode(episode: Episode, dir: string, onProgress?: (progress: Progress) => void): Promise<FileInfo> {
    return utils.downloadEpisode(this.loadercdnToken, episode, dir, null, onProgress)
  }

  async downloadSeason(season: Season, dir: string, onProgress?: Function): Promise<FileInfo[]> {
    let lastEpisodeProgress = null
    const fileInfos: FileInfo[] = []
    const progress: Progress = { count: 0, max: season.episodes.length, title: season.title, procent: 0 }

    if (season.episodes.length === 0) console.log(
      'В сезоне нет ни одного эпизода, возможно вы забыли вызвать loadEpisodes?'
    )

    for (const episode of season.episodes) {
      const fileInfo = await this.downloadEpisode(episode, dir, (episodeProgress: Progress) => {
        lastEpisodeProgress = episodeProgress
        onProgress(process, lastEpisodeProgress)
      })
      
      // Update progress
      progress.count++
      progress.procent = progress.count / progress.max * 100
      onProgress(process, lastEpisodeProgress)

      // Just for the referance
      fileInfo.episode = episode

      fileInfos.push(fileInfo)
    }

    return fileInfos
  }
}