import * as request from 'request'
import { JSDOM } from 'jsdom'

import { Episode } from '../models'

export async function getEpisode(url: string): Promise<Episode> {
  const body: string = <string> await (new Promise((resolve, reject) => 
    request.get(url, {}, async (err, res, body) => {
      if (err) return reject(err)
      resolve(body)
    }))
  )

  const { window: { document }} = new JSDOM(body)

  const alias: string[] = Array.from(document.querySelectorAll('.contentnav a')).map((n: any) => n.textContent.trim().toLowerCase())
  const urls: string[] = Array.from(document.querySelectorAll('.contentVideo iframe')).map((f: any) => f.src)
  const title: string = document.querySelector('.content h1').textContent.trim()
  const art: string = 'http://cartoonsub.com/' + document.querySelector('.art img').src
  
  const episode: Episode = { links: [], title, art }

  alias.forEach((a, i) => {
    episode.links[i] = { type: a, url: urls[i] }
  })

  return episode
}
