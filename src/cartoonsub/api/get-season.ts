import * as request from 'request'
import { URL } from 'url'
import { JSDOM } from 'jsdom'

import { getEpisode } from './'
import { Season, Episode } from '../models'

export async function getSeason(seasonUrl: string, loadEpisodes: boolean = false): Promise<Season> {
  const body: string = <string> await (new Promise((resolve, reject) =>
    request.get(seasonUrl, {}, async (err, res, body) => {
      if (err) return reject(err)
      resolve(body)
    }))
  )

  const { window: { document }} = new JSDOM(body)
  const origin = (new URL(seasonUrl)).origin + '/'

  const episodeUrls: string[] = Array.from(document.querySelectorAll('.page_nav > a')).map((a: any) => origin + a.href).reverse()
  const name: string = document.querySelector('.navifationPath').innerHTML.split('→').slice(-1)[0].trim()
  const title: string = (<any> Array.from(document.querySelectorAll('.navifationPath a')).filter((a: any) => a.href.includes('id_serials='))[0]).title
  const episodes: Episode[] = []

  if (loadEpisodes) {
    for (const episodeUrl of episodeUrls) episodes.push(await getEpisode(episodeUrl))
  }

  return ({ episodes, title, name, episodeUrls })
}
