import * as request from 'request'

import { FileInfo } from '../models/'

export async function getRealUrl(hostingLink: string, API_KEY: string): Promise<FileInfo> {
  const options: any = {
    url: 'https://loadercdn.io/api/v1/create',
    form: {
      key: API_KEY,
      link: hostingLink
    }
  }

  const body: string = <string> await (new Promise((resolve, reject) => {
    request.post(options, (err, httpResponse, body: string) => {
      if (err) return reject(err)
      resolve(body)
    })
  }));

  const info = JSON.parse(body)
  const fileInfo: FileInfo = {
    url: info.formats[0].url,
    fileName: info.formats[0].filename,
    format: info.formats[0].format,
    headers: info.headers
  }

  return fileInfo
}