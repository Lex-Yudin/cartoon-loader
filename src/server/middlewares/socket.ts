import { get } from 'lodash'
import { server } from '../../index'

interface SocketMessage {
  type: 'error' | 'warning' | 'info',
  message: string,
  data?: object
}

function sendMessage(message: SocketMessage, clientId: string) {
  if (!clientId) return
  server.io.to(clientId).emit('message', message);
}

function sendMessageAll(message: SocketMessage) {
  server.io.sockets.emit('message', message);
}

export function SocketApiMiddleware(req, res, next): void {
  const session = get(req, 'headers.socket', null)
  const isAvalible = session !== null

  res.infoAll = (message: string, data?: object) => { sendMessageAll({ type: 'info', message, data }); return res }
  res.warnAll = (message: string, data?: object) => { sendMessageAll({ type: 'warning', message, data }); return res }
  res.errorAll = (message: string, data?: object) => { sendMessageAll({ type: 'error', message, data }); return res }

  res.info = (message: string, data?: object) => { sendMessage({ type: 'info', message, data }, session); return res }
  res.warn = (message: string, data?: object) => { sendMessage({ type: 'warning', message, data }, session); return res }
  res.error = (message: string, data?: object) => { sendMessage({ type: 'error', message, data }, session); return res }

  next()
}