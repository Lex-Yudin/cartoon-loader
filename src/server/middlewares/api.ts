interface ApiResponce {
  data: Array<any> | object
}

function api (response: Array<any> | object): ApiResponce {
  const obj: ApiResponce = { data: response }

  return obj
}

export const ApiMiddleware = (req, res, next) => {
  res.api = (data) => res.json(api(data))
  next()
}