import { server } from '../../../'

export function read (req, res): void {
  const tasks = server.taskManager.tasks

  res.api(tasks)
}