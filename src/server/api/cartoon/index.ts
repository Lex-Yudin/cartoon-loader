import { Router } from 'express'
import { downloadEpisode } from './download-episode'

export const cartoon = Router()
  .post('/downloadEpisode', downloadEpisode)