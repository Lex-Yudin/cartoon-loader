import * as path from 'path'
import { server } from '../../../'
import { DOWNLOAD_DIR } from '../../constants'

export async function downloadEpisode (req, res): Promise<void> {
  const { link, primary = 'оригинал', okey = [] } = req.body

  if (!link) return res.status(400).json()

  let episode, analizedEpisodes

  try {
    episode = await server.cartoonsubService.loadEpisode(link)
    analizedEpisodes = server.cartoonsubService.analizeEpisode({ errorLvl: 3, okey, primary, filter: true }, [episode])
  } catch (err) {
    return res.status(500).error(`Произошла ошибка (${err.message})`).api({})
  }

  // TODO: Вернуть возможные значения
  if (analizedEpisodes.length === 0) return res.status(404).warn('По переданным предпочтениям не найдено видео')

  const task = server.taskManager.createTask((resolve, reject, onProgress) => {
    server.cartoonsubService.downloadEpisode(analizedEpisodes[0], DOWNLOAD_DIR, (info) => onProgress(info.procent))
      .then((file) => resolve(file))
      .catch((err) => reject(err))
  }, analizedEpisodes[0])

  res.api({ task, episode: analizedEpisodes[0] })
}