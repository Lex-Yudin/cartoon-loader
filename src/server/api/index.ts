import { Router } from 'express'

import { cartoon } from './cartoon'
import { task } from './task';

export const api = Router()
  .use('/cartoon', cartoon)
  .use('/task', task)