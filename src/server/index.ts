import * as path from 'path'
import * as http from 'http'
import * as express from 'express'
import * as socketIO from 'socket.io'
import * as bodyParser from 'body-parser'

import { api } from './api';
import { TaskManager } from '../task-manager';
import { ApiMiddleware, SocketApiMiddleware } from './middlewares';
import { CartoonsubService } from '../cartoonsub'

export class Server {
  private app: any

  // Socket
  public io: any
  private ioServer: any

  public cartoonsubService: CartoonsubService
  public taskManager: TaskManager

  constructor() {
    this.app = express()

    this.cartoonsubService = new CartoonsubService(process.env.API_KEY)
    this.taskManager = new TaskManager((task) => this.io.sockets.emit('task', task))

    this.setup()
    this.setupSocket()
  }

  setup() {
    // EJS
    this.app.set('view engine', 'ejs');
    this.app.set('views', path.join(__dirname, '/views'));

    // Render
    this.app.get('/', (req, res) => res.render('index'));
    this.app.get('/:page', (req, res) => res.render(req.params.page));

    // API
    this.app.use(ApiMiddleware, SocketApiMiddleware)
    this.app.use(bodyParser.json())
    this.app.use('/api', api)
  }

  setupSocket() {
    this.ioServer = new http.Server(this.app)
    this.io = socketIO(this.ioServer)

    this.io.on('connection', socket => console.log('[Socket] User connected', Object.keys(this.io.sockets.sockets)));
  }

  start() {
    const port = process.env.PORT || 8080
    const socketPort = process.env.SOCKET_PORT || 3000

    this.app.listen(port, () => console.log(`Web server at ${port}`))
    this.ioServer.listen(socketPort, () => console.log(`Socket at ${socketPort}\n`));
  }
}
