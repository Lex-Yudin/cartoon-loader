// import season3 from '../data/season3.json'
import * as path from 'path'
import { Season, Progress } from '../src/cartoonsub/models'
import { cartoonsubService } from '../src/cartoonsub'

const season3Url = 'http://cartoonsub.com/allseries.php?id_season=83&id_serials=49'

console.log('Downloading Season')
cartoonsubService.loadSeason(season3Url).then((season: Season) => {
  console.log('Loading Episodes')
  cartoonsubService.loadEpisodes(season).then((season: Season) => {
    console.log('Downloading Episode one')
    const dir = path.join(__dirname, '..', 'data')
    const episideOne = cartoonsubService.analizeEpisode({
      primary: 'оригинал',
      okey: ['субтитры']
    }, [season.episodes[0]])[0]

    if (!episideOne) return console.log('FUCK')

    cartoonsubService.downloadEpisode(episideOne, dir, (progress: Progress) => {
      console.log(`[Downloading] (${Math.floor(progress.procent)}%) - ${progress.title}`)
    }).then(() => {
      console.log('Done!')
    })
  })
})